import React from "react";
import { Table, TableRows } from "components/Table";
import { AddButton, DeleteButton } from "./components/Buttons";
import SearchBox from "./components/SearchBox";
import { headings } from "utils/stubs";
import { Row, Col } from "reactstrap";
const Home = () => {
  return (
    <div className="mt-5">
      <Table STORE_REF="avengers">
        <Row className="mb-3">
          <Col lg={{ size: 4, offset: 5 }} className="text-right">
            {" "}
            <DeleteButton className="mr-3" />
            <AddButton />
          </Col>
          <Col lg="3">
            <SearchBox />
          </Col>
        </Row>

        <TableRows headings={headings} />
      </Table>
    </div>
  );
};

export default Home;
