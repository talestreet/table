import React from "react";
import { Input, InputGroup, InputGroupText, InputGroupAddon } from "reactstrap";
import { connectSearch } from "components/Table";
import style from "./_searchBox.module.scss";
const SearchBox = ({ search, searchQuery }) => {
  return (
    <InputGroup className={style.root}>
      <InputGroupAddon addonType="prepend" className={style.prepend}>
        <InputGroupText>
          <i className="fas fa-search"></i>
        </InputGroupText>
      </InputGroupAddon>
      <Input
        className={style.textbox}
        placeholder="Search"
        onChange={(e) => {
          search(e.target.value);
        }}
        value={searchQuery || ""}
      />
    </InputGroup>
  );
};

export default connectSearch(SearchBox);
