import React from "react";
import { Button } from "reactstrap";
import { connectAdd } from "components/Table";
const AddButton = ({ openAddModal, className }) => {
  return (
    <Button
      color="primary"
      className={`${className} custom-buttons`}
      onClick={openAddModal}
    >
      Add Record
    </Button>
  );
};
const connectedAdd = connectAdd(AddButton);
export default connectedAdd;
