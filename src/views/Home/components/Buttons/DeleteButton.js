import React from "react";
import { Button } from "reactstrap";
import { connectDelete } from "components/Table";
const DeleteButton = ({ remove, selected = {}, className }) => {
  const numSelected = Object.keys(selected).length;
  if (numSelected < 1) return null;
  return (
    <Button
      color="danger"
      className={`${className} custom-buttons`}
      onClick={remove}
    >
      Delete({numSelected})
    </Button>
  );
};
const connectedDelete = connectDelete(DeleteButton);
export default connectedDelete;
