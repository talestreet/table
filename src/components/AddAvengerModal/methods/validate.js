import validateEmail from "./validateEmail";
const validate = (values) => {
  let errors = {};
  if (!values.firstName || values.firstName.length < 2) {
    errors.firstName = "First Name should be atleast 2 characters in length";
  }
  if (!values.lastName || values.lastName.length < 2) {
    errors.lastName = "Last Name should be atleast 2 characters in length";
  }
  if (!values.superHeroName || values.superHeroName.length < 2) {
    errors.superHeroName =
      "SuperHero Name should be atleast 2 characters in length";
  }
  if (!values.age || values.age < 1) {
    errors.age = "Age should be greater than 0";
  }
  if (!values.email || !validateEmail(values.email)) {
    errors.email = "Invalid Email Address";
  }
  if (!values.gender) {
    errors.gender = "Please select one";
  }
  return errors;
};

export default validate;
