import React, { useState } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { validate } from "./methods";
import { TextInput } from "components/";
import style from "./_addNewAvengerModal.module.scss";
const AddAvengerModal = ({ isOpen, cancel, add }) => {
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const handleChange = (_key, value) => {
    if (_key === "age") value = +value;
    setErrors({});
    setValues({ ...values, [_key]: value });
  };
  const done = () => {
    const errors = validate(values);
    if (Object.keys(errors).length > 0) {
      setErrors(errors);
      return;
    }
    add(values);
  };
  return (
    <Modal isOpen={isOpen} toggle={cancel}>
      <ModalHeader toggle={cancel}>Add a New Avenger</ModalHeader>
      <ModalBody>
        <Form>
          <Row>
            <Col>
              {" "}
              <TextInput
                _key={"firstName"}
                value={values.firstName}
                type="text"
                label="First Name"
                placeholder="First Name"
                handleChange={handleChange}
                errors={errors}
              />
            </Col>
            <Col>
              {" "}
              <TextInput
                _key={"lastName"}
                value={values.lastName}
                type="text"
                label="Last Name"
                placeholder="Last Name"
                handleChange={handleChange}
                errors={errors}
              />
            </Col>
          </Row>

          <TextInput
            _key={"superHeroName"}
            value={values.superHeroName}
            type="text"
            label="Super Hero Name"
            placeholder="Super Hero Name"
            handleChange={handleChange}
            errors={errors}
          />
          <TextInput
            _key={"email"}
            value={values.email}
            type="email"
            label="Email"
            placeholder="Email"
            handleChange={handleChange}
            errors={errors}
          />
          <Row>
            <Col>
              {" "}
              <TextInput
                _key={"age"}
                value={values.age}
                type="number"
                label="Age"
                placeholder="Age"
                handleChange={handleChange}
                errors={errors}
              />
            </Col>
            <Col>
              <FormGroup>
                <Label className={style.label} for="genderSelect">
                  Gender
                </Label>
                <Input
                  type="select"
                  name="select"
                  onChange={(e) => handleChange("gender", e.target.value)}
                  invalid={errors.gender}
                >
                  <option>Select Gender</option>
                  <option value="m">Male</option>
                  <option value="f">Female</option>
                </Input>
                <div className="invalid-feedback">{errors.gender}</div>
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </ModalBody>
      <ModalFooter>
        <Button className={style.button} color="secondary" onClick={cancel}>
          Cancel
        </Button>
        <Button className={style.button} color="primary" onClick={done}>
          Done
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AddAvengerModal;
