import React from "react";
import { FormGroup, Label, Input } from "reactstrap";
import style from "./_textInput.module.scss";
const TextInput = ({
  type,
  label,
  placeholder,
  _key,
  handleChange,
  value,
  errors,
}) => {
  return (
    <FormGroup>
      <Label className={style.label} for={label.replace(" ", "_")}>
        {label}
      </Label>
      <Input
        value={value}
        type={type}
        name="firstName"
        placeholder={placeholder}
        onChange={(e) => {
          handleChange(_key, e.target.value);
        }}
        invalid={errors[_key]}
      />
      <div className="invalid-feedback">{errors[_key]}</div>
    </FormGroup>
  );
};
export default TextInput;
