import React, { useState } from "react";
import localStore from "utils/localStore";
import { AddAvengerModal } from "components";
import { sortList, searchList } from "./methods";
const TableContext = React.createContext();
const TableConsumer = TableContext.Consumer;
const Table = ({ children, headings, STORE_REF }) => {
  const [list, setList] = useState(localStore.get(STORE_REF) || []);
  const [selected, setSelected] = useState({});
  const [isAddModalOpen, setAddModal] = useState(false);
  const [sortedBy, setSort] = useState(null);
  const [searchQuery, setSearchQuery] = useState(null);
  const add = (avenger) => {
    const updatedList = [
      ...list,
      { ...avenger, sn: list.length + 1, id: Date.now() },
    ];
    setList(updatedList);
    setAddModal(false);
    localStore.update(STORE_REF, updatedList);
  };
  const openAddModal = () => {
    setAddModal(true);
  };
  const cancelAdd = () => {
    setAddModal(false);
  };
  const search = (value) => {
    setSearchQuery(value);
  };
  const remove = () => {
    const updatedList = list.filter(({ id }) => !selected[id]);
    setList(updatedList);
    setSelected({});
    localStore.update(STORE_REF, updatedList);
  };
  const toggleSelect = (id, value) => {
    if (value) {
      setSelected({ ...selected, [id]: true });
    } else {
      const newSelected = { ...selected };
      delete newSelected[id];
      setSelected(newSelected);
    }
  };
  const sort = (_key) => {
    if (!sortedBy || sortedBy._key !== _key) {
      setSort({
        _key,
        order: "asc",
      });
      return;
    }

    if (sortedBy.order === "desc") {
      setSort(null);
      return;
    }

    if (sortedBy.order === "asc") {
      setSort({ ...sortedBy, order: "desc" });
      return;
    }
  };

  const searchedList = searchList(list, searchQuery);
  const sortedList = sortList(searchedList, sortedBy);
  const context = {
    list: sortedList,
    openAddModal,
    remove,
    toggleSelect,
    sort,
    sortedBy,
    search,
    searchQuery,
    selected,
  };
  return (
    <>
      <TableContext.Provider value={context}>{children}</TableContext.Provider>
      <AddAvengerModal cancel={cancelAdd} isOpen={isAddModalOpen} add={add} />
    </>
  );
};

export { Table, TableContext, TableConsumer };
