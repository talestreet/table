import React from "react";
import { TableConsumer } from "./../TableContext";
const connectAdd = (ConnectedComponent) => {
  return (props) => (
    <TableConsumer>
      {({ openAddModal }) => (
        <ConnectedComponent {...props} openAddModal={openAddModal} />
      )}
    </TableConsumer>
  );
};

const connectDelete = (ConnectedComponent) => {
  return (props) => (
    <TableConsumer>
      {({ remove, selected }) => (
        <ConnectedComponent {...props} remove={remove} selected={selected} />
      )}
    </TableConsumer>
  );
};
const connectSearch = (ConnectedComponent) => {
  return (props) => (
    <TableConsumer>
      {({ search, searchQuery }) => (
        <ConnectedComponent
          {...props}
          search={search}
          searchQuery={searchQuery}
        />
      )}
    </TableConsumer>
  );
};
export { connectAdd, connectDelete, connectSearch };
