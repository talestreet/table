import TableRows from "./TableRows";
import TableHeadings from "./TableHeadings";
import { Table, TableContext } from "./TableContext";
import { connectAdd, connectDelete, connectSearch } from "./connectors";
export {
  Table,
  TableRows,
  TableContext,
  TableHeadings,
  connectAdd,
  connectDelete,
  connectSearch,
};
