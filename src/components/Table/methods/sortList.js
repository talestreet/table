const sortList = (list, sortedBy) => {
  if (!sortedBy) return list;
  const { _key, order } = sortedBy;
  const updatedList = [...list];
  updatedList.sort((a, b) => {
    if (a[_key] < b[_key]) {
      return order === "asc" ? -1 : 1;
    }
    if (a[_key] > b[_key]) {
      return order === "asc" ? 1 : -1;
    }
    return 0;
  });
  return updatedList;
};
export default sortList;
