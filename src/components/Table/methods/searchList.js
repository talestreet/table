const searchList = (list, searchQuery) => {
  if (!searchQuery) searchQuery = "";
  searchQuery = String(searchQuery).toLowerCase();
  const updatedList = list.filter((avenger) => {
    const values = Object.values(avenger).filter((value) => {
      const subStringIndex = String(value).toLowerCase().search(searchQuery);
      return subStringIndex > -1;
    });
    return values.length > 0;
  });
  return updatedList;
};
export default searchList;
