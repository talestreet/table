import React, { useContext } from "react";
import { Table as ReactStrapTable } from "reactstrap";
import { TableContext } from "./index";
import TableHeadings from "./TableHeadings";
import style from "./_table.module.scss";
const TableRows = ({ headings }) => {
  const { list, toggleSelect, selected } = useContext(TableContext);
  return (
    <ReactStrapTable striped>
      <TableHeadings headings={headings} />

      <tbody className={style.body}>
        {list.length < 1 && (
          <tr className={style.alert}>
            <td colSpan="8">No records found</td>
          </tr>
        )}
        {list.map((data, index) => {
          const isSelected = !!selected[data.id];
          return (
            <tr key={data.id} className={isSelected ? style.selected : ""}>
              <th
                className="cursor"
                onClick={(e) => {
                  toggleSelect(data.id, !isSelected);
                }}
              >
                {
                  <i
                    className={`${
                      isSelected ? "fas fa-check-square" : "far fa-square"
                    } `}
                  ></i>
                }
              </th>
              <th scope="row">{data.sn}</th>
              <td>{data.firstName}</td>
              <td>{data.lastName}</td>
              <td>{data.superHeroName}</td>
              <td>{data.email}</td>
              <td className="text-uppercase">{data.gender}</td>
              <td>{data.age}</td>
            </tr>
          );
        })}
      </tbody>
    </ReactStrapTable>
  );
};
export default TableRows;
