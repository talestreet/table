import React, { useContext } from "react";
import { TableContext } from "./index";
import style from "./_table.module.scss";
const TableHeadings = ({ headings }) => {
  const { sort, sortedBy } = useContext(TableContext);
  return (
    <thead className={style.headings}>
      <tr>
        <th></th>
        {headings.map(({ title, _key, allowSorting }) => {
          const isSorted =
            sortedBy && sortedBy._key === _key ? sortedBy.order : null;
          if (!allowSorting) {
            return <th key={_key}>{title}</th>;
          }
          return (
            <th className={style.heading} key={_key} onClick={() => sort(_key)}>
              {title}{" "}
              {isSorted === "asc" && (
                <i className="fas fa-long-arrow-alt-up"></i>
              )}
              {isSorted === "desc" && (
                <i className="fas fa-long-arrow-alt-down"></i>
              )}
            </th>
          );
        })}
      </tr>
    </thead>
  );
};
export default TableHeadings;
