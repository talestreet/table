import React from "react";
import { Container } from "reactstrap";
import Home from "views/Home";
import { populateDummyData } from "utils";
import { Footer } from "components";
function App() {
  populateDummyData();
  return (
    <Container>
      <Home />
      <Footer />
    </Container>
  );
}

export default App;
