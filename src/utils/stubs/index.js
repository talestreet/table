const headings = [
  {
    title: "#",
    _key: "index",
    allowSorting: false,
  },
  {
    title: "First Name",
    _key: "firstName",
    allowSorting: true,
  },
  {
    title: "Last Name",
    _key: "lastName",
    allowSorting: true,
  },
  {
    title: "Super Hero Name",
    _key: "superHeroName",
    allowSorting: true,
  },
  {
    title: "Email",
    _key: "email",
    allowSorting: true,
  },
  {
    title: "Gender",
    _key: "gender",
    allowSorting: true,
  },
  {
    title: "Age",
    _key: "age",
    allowSorting: true,
  },
];
const AVENGERS = [
  {
    firstName: "Tony",
    lastName: "Stark",
    superHeroName: "Iron Man",
    email: "tony@avengers.com",
    gender: "F",
    age: 53,
    id: "a1",
    sn: 1,
  },
  {
    firstName: "Natasha",
    lastName: "Romanova",
    superHeroName: "Black Widow",
    email: "natasha@avengers.ccom",
    gender: "F",
    age: 22,
    id: "a2",
    sn: 2,
  },
  {
    firstName: "Steve",
    lastName: "Rogers",
    superHeroName: "Captain America",
    email: "steve@avengers.com",
    gender: "M",
    age: 99,
    id: "a3",
    sn: 3,
  },
];
export { headings, AVENGERS };
