import localStore from "./localStore";
import { AVENGERS } from "./stubs";
const populateDummyData = (override) => {
  const data = localStore.get("avengers");
  if (data && !override) return;
  localStore.update("avengers", AVENGERS);
};
export { populateDummyData };
